# Version 0.3.1 - The Great Particle Purge

## Changes

* Forced 60 FPS for the time being.
* Made the tutorial easier on the dashes.
* New Barrier Sprite.
* Player speed values have been lowered by a bit in order to increase control.
* Demo Stage remade now with SECRET AREA!

## Removed

* Most of the ingame particles due to performance errors.

# Version 0.3

## New Stuff

* New Inca Tileset
* Charger: An ancient object that chargers the player when touched.
* New Powerup: Dashing! (Can only be used while charged).
* Switches and Doors!

## Changes

* Added new stuff to tutorial (no new level right now D:)

## Fixes

* Fix getting stuck on tileset when longjumping

# Bugfixes

* Hopefully you won't get stuck anymore after long jumping

# Hotfix 0.2.1

* Added WASD to keybindings

# Version 0.2

## New Stuff

* Coyote Jump
* Jump Buffering
* Demo Stage Rework

## Changes

* Reduced normal/crouched jump heights
* Slides have no friction (as long as you hold the direction)

# Version 0.1

* Initial Demo Release