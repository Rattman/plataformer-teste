extends Label

var Player

# Called when the node enters the scene tree for the first time.
func _ready():
	Player = get_tree().root.get_child(0).get_child(1)

func _process(delta):
	self.text = "X = " + str(Player.speed.x) + "\nY = " + str(Player.speed.y) + \
				"\nState = " + Player.physic
