extends Label

var time_start = 0
var time_now = 0
var started = false

func _process(delta):
	if started:
	    time_now = OS.get_unix_time()
	    var elapsed = time_now - time_start
	    var minutes = elapsed / 60
	    var seconds = elapsed % 60
	    var str_elapsed = "%02d : %02d" % [minutes, seconds]
	   	set_text(str_elapsed)

func _on_start_line_start():
	started = true
	self.show()
	time_start = OS.get_unix_time()
	time_now = 0

func _on_end_line_end():
	started = false
