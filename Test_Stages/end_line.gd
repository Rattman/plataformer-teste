extends Area2D

signal end()

func _on_end_line_body_entered(body):
	emit_signal("end")
