extends Camera2D

var Player = null

func _ready():
	Player = get_parent().get_child(1)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	self.position = Player.position
