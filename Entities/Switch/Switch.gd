extends Area2D

export(Array, NodePath) var activates : Array = []
export(bool) var charge : bool = false

const GUIDE_PATH = "res://Entities/Switch/Components/Switch_Guide.tscn"

var active : bool = false
var Guide_Res : Resource = null

func _ready():
	if charge:
		$body.set_color(Color("362e6e"))

	Guide_Res = load(GUIDE_PATH)

func _on_Switch_body_entered(body):
	if not active:
		if charge:
			if body.charged:
				$Animation.play("Charged")
				self.active()

		else:
			$Animation.play("Pressed")
			self.active()

func active() -> void:
	active = true
	for object in activates:
		get_node(object).activate()
		var new_guide = Guide_Res.instance()
		new_guide.setup(get_node(object))
		self.add_child(new_guide)