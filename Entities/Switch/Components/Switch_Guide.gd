extends Particles2D

var object : Node2D = null

func setup(goal : Node2D) -> void:
	self.object = goal

func _ready():
	$Tween.interpolate_property(self, "global_position", self.global_position,
								object.global_position, 1, Tween.TRANS_LINEAR,
								Tween.TRANS_LINEAR)
	$Tween.start()

func _on_Tween_tween_all_completed():
	self.emitting = false
	$Open.emitting = true
	$Suicide.start()

func _on_Suicide_timeout():
	self.queue_free()
