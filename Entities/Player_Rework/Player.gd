extends KinematicBody2D

#Constants
enum {LEFT = -1, NONE, RIGHT}

#Data/Components
var physics : Dictionary = {}
var dash_particles : PackedScene = null

#Signals
signal request_anim(anim)

#Directions
var move_dir : int = 0
var input_dir : int = 0

#Attributes
var ready : bool = false
var speed = Vector2(0, 0)
var charged : bool = false setget set_charge
var animation : String setget set_animation
var physic : String setget change_physics

#Properties
var jump_buffered : bool = false
var dash_buffered : bool = false
var is_crouched : bool = false

#Ready
func _ready():
	#Load Physics
	var file : File = File.new()
	file.open("res://Entities/Player_Rework/player_physics.json", file.READ)
	physics = parse_json(file.get_as_text())
	file.close()

	#Load Dash Particles
	dash_particles = load("res://Entities/Player_Rework/Components/dash.tscn")

	#Set ready to true so the FSM may continue
	ready = true

#Physics
func change_physics(state : String) -> void:
	assert(physics.has(state))
	$Physics_Processor.change_physics(physics[state])
	physic = state

	if state == "Crouch" or state == "Slide" or state == "Forced_Crouch":
		self.is_crouched = true
	else:
		self.is_crouched = false

func dash(x : float, y : float) -> void:
	var dash_speed = physics["Dash"]["MAX_SPEED"]
	speed.x = x*dash_speed
	speed.y = y*dash_speed
	self.add_child(dash_particles.instance())

	dash_buffered = false
	$Dash_Buffer.stop()

	self.charged = false

func jump(x : float, y: float) -> void:
	speed.x -= x*physics["Jump"]["WIDTH"]*(-move_dir)
	speed.y -= y*physics["Jump"]["HEIGHT"]

	jump_buffered = false
	$Jump_Buffer.stop()

func can_stand() -> bool:
	return len($Stand_Space.get_overlapping_bodies()) == 0

# Properties

func set_charge(value : bool) -> void:
	if value == true:
		charged = true
		self.animation = "Charge"
		$charge.emitting = true
	else:
		charged = false
		$charge.emitting = false

#Animations
func set_animation(anim : String) -> void:
	emit_signal("request_anim", anim)
	animation = anim

#Inputs
func _input(event):

	if event.is_action_pressed("player_left"):
		input_dir = LEFT

	elif event.is_action_pressed("player_right"):
		input_dir = RIGHT

	elif event.is_action_released("player_left"):
		if Input.is_action_pressed("player_right"):
			input_dir = RIGHT
		else:
			input_dir = NONE

	elif event.is_action_released("player_right"):
		if Input.is_action_pressed("player_left"):
			input_dir = LEFT
		else:
			input_dir = NONE

	elif event.is_action_pressed("player_jump"):
		jump_buffered = true
		$Jump_Buffer.start()

	elif event.is_action_pressed("player_dash"):
		dash_buffered = true
		$Dash_Buffer.start()

#Connected Signals
func _on_Jump_Buffer_timeout():
	jump_buffered = false

func _on_Dash_Buffer_timeout():
	dash_buffered = false

#Update (Error Checking)
func _process(delta):
	#Crouchs the Player if he gets stuck inside a wall
	#if Input.is_action_just_pressed("ui_down"):
	if not self.is_crouched and len($Stand_Space.get_overlapping_bodies()) > 0:
		$States.changeStateTo("Forced_Crouch")
