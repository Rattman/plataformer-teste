extends AnimationPlayer

onready var Player = get_parent()

# Signal taker
func _on_Player_request_anim(anim : String):
	if anim == "Crouch":
		self.crouch()

	elif anim == "Stand":
		self.stand()

	elif anim == "Jump":
		self.jump()

	elif anim == "Wall_Jump":
		self.wall_jump()

	elif anim == "Long_Jump":
		self.long_jump()

	elif anim == "Crouch_Jump":
		self.crouch_jump()

	elif anim == "Dash":
		self.dash()

	elif anim == "Charge":
		self.charge()

	else:
		print("No Animation")
		assert(false)

#Animation Functions
func jump():
	self.play("Jump")

func wall_jump():
	self.play("Wall_Jump")

func crouch_jump():
	if not Player.charged:
		self.play("Crouch_Jump")
	else:
		self.play("Charged_Crouch_Jump")

func crouch():
	self.play("Crouch")

func stand():
	self.play_backwards("Crouch")

func charge():
	self.play("Charge")

func dash():
	self.play("Dash")

func long_jump():
	if Player.move_dir > 0:
		self.play("Long_Jump")
	else:
		self.play("Long_Jump(left)")
