extends "res://addons/net.kivano.fsm/content/FSMState.gd";
################################### R E A D M E ##################################
# For more informations check script attached to FSM node
#
#

##################################################################################
#####  Variables (Constants, Export Variables, Node Vars, Normal variables)  #####
######################### var myvar setget myvar_set,myvar_get ###################

var Player = null
var Coyote = null

##################################################################################
#########                       Getters and Setters                      #########
##################################################################################
#you will want to use those
func getFSM(): return fsm; #defined in parent class
func getLogicRoot(): return logicRoot; #defined in parent class

##################################################################################
#########                 Implement those below ancestor                 #########
##################################################################################
#you can transmit parameters if fsm is initialized manually
func stateInit(inParam1=null,inParam2=null,inParam3=null,inParam4=null, inParam5=null):
	Player = getLogicRoot()
	Coyote = $"../Coyote_Chance"

#when entering state, usually you will want to reset internal state here somehow
func enter(fromStateID=null, fromTransitionID=null, inArg0=null,inArg1=null, inArg2=null):
	Player.physic = "Jumping"

	if fromStateID == "Coyote_Chance":
		fromStateID = Coyote.state
		Player.speed = Coyote.speed
		Player.move_dir = Coyote.move_dir

	if(fromStateID == "On_Ground"):
		Player.animation = "Jump"
		Player.jump(0, 0.8)

	elif(fromStateID == "Wall_Sliding"):
		Player.animation = "Wall_Jump"
		Player.jump(-0.5, 0.9)

	elif(fromStateID == "Crouching"):
		Player.animation = "Crouch_Jump"
		Player.jump(0, 1)

	elif(fromStateID == "Sliding"):
		Player.animation = "Long_Jump"
		Player.jump(1, 0.6)

#when updating state, paramx can be used only if updating fsm manually
func update(deltaTime, param0=null, param1=null, param2=null, param3=null, param4=null):
	pass

#when exiting state
func exit(toState=null):
	pass

##################################################################################
#########                       Connected Signals                        #########
##################################################################################

##################################################################################
#########     Methods fired because of events (usually via Groups interface)  ####
##################################################################################

##################################################################################
#########                         Public Methods                         #########
##################################################################################

##################################################################################
#########                         Inner Methods                          #########
##################################################################################

##################################################################################
#########                         Inner Classes                          #########
##################################################################################
