extends Area2D

var ready : bool = true

func _on_Charger_body_entered(body):
	if self.ready:
		body.charged = true
		self.ready = false
		$Animation.play("Recharging")
		$Recharge.start()

func _on_Recharge_timeout():
	ready = true
	for body in self.get_overlapping_bodies():
		body.charged = true
		self.ready = false
		$Animation.play("Recharging")
		$Recharge.start()
		return