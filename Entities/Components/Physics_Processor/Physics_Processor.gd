extends Node

var Player : KinematicBody2D = null

#Speed Decrease Modes
enum {LIMIT, LINEAR, NONE}

# Horizontal Movement
var ACCEL : float = 0
var MAX_SPEED : float = 0
var STOP_SPEED : float = 0
var FRICTION : float = 0

var SPD_DECREASE : float = LIMIT

# Vertical Movement
var GRAVITY : float = 0
var TERMINAL_SPEED : float = 0

func change_physics(change : Dictionary) -> void:
	if change.has("ACCEL"):
		self.ACCEL = change["ACCEL"]
	else:
		self.ACCEL = 0

	if change.has("MAX_SPEED"):
		self.MAX_SPEED = change["MAX_SPEED"]
	else:
		self.MAX_SPEED = 0

	if change.has("SPD_DECREASE"):
		self.SPD_DECREASE = change["SPD_DECREASE"]
	else:
		self.SPD_DECREASE = LIMIT

	if change.has("STOP_SPEED"):
		self.STOP_SPEED = change["STOP_SPEED"]
	else:
		self.STOP_SPEED = 2

	if change.has("FRICTION"):
		self.FRICTION = change["FRICTION"]
	else:
		self.FRICTION = 0

	if change.has("GRAVITY"):
		self.GRAVITY = change["GRAVITY"]
	else:
		self.GRAVITY = 0

	if change.has("TERMINAL_SPEED"):
		self.TERMINAL_SPEED = change["TERMINAL_SPEED"]
	else:
		self.TERMINAL_SPEED = 0

func _ready():
	Player = self.get_parent()

func _physics_process(delta):
	var speed : Vector2 = Player.speed
	var move_dir = Player.move_dir
	var input_dir = Player.input_dir

	#Process Horizontal Movement
	speed.x += ACCEL*input_dir

	if move_dir != input_dir:
		speed.x *= FRICTION

	if abs(speed.x) > MAX_SPEED:
		if SPD_DECREASE == LIMIT:
			speed.x = MAX_SPEED*sign(speed.x)

		elif SPD_DECREASE == LINEAR:
			speed.x = max(MAX_SPEED, abs(speed.x) * FRICTION)*sign(speed.x)

	if abs(speed.x) < STOP_SPEED: speed.x = 0

	if speed.x > 0: Player.move_dir = Player.RIGHT
	elif speed.x < 0: Player.move_dir = Player.LEFT
	else: Player.move_dir = Player.NONE

	#Process Vertical Movement
	speed.y += GRAVITY
	speed.y = min(speed.y, TERMINAL_SPEED)

	#Move
	Player.move_and_slide(speed, Vector2(0, -1))

	Player.speed = speed

