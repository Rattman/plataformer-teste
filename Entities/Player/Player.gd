extends KinematicBody2D

const CONST = preload("PLAYER_CONSTANTS.gd")

enum dir {NONE, LEFT, RIGHT}

var moving = dir.NONE
var speed = Vector2(0,0)

var wall_dir = dir.NONE

## Signals

### Getters/Setters

func get_mv_dir():
	return self.moving

func get_v_speed():
	return self.speed.x

func set_v_speed(value):
	self.speed.x = value

func get_h_speed():
	return self.speed.y

func set_h_speed(value):
	self.speed.y = value

func set_color(color):
	return
	$body.set_color(color)

### System funcs

func _input(event):

	if event.is_action_pressed("ui_left"):
		moving = dir.LEFT

	elif event.is_action_pressed("ui_right"):
		moving = dir.RIGHT

	elif event.is_action_released("ui_left"):
		if Input.is_action_pressed("ui_right"):
			moving = dir.RIGHT
		else:
			moving = dir.NONE

	elif event.is_action_released("ui_right"):
		if Input.is_action_pressed("ui_left"):
			moving = dir.LEFT
		else:
			moving = dir.NONE

func _physics_process(delta):
	move_and_slide(speed, Vector2(0,-1))