extends Node

const CONST = preload("PLAYER_CONSTANTS.gd")
const STANDING_SCALE = Vector2(1,1)
const CROUCHING_SCALE = Vector2(1,0.5)

onready var Player = get_parent()

func _process(delta):
	var speed = Player.speed
	var scale_x = STANDING_SCALE.x
	var scale_y = STANDING_SCALE.y

	if Input.is_action_pressed("ui_down"):
		scale_x = CROUCHING_SCALE.x
		scale_y = CROUCHING_SCALE.y

	scale_x /= 1-0.07*(abs(speed.x)/CONST.RUN_MAX_SPEED)
	scale_y /= 1+0.07*(abs(speed.x)/CONST.RUN_MAX_SPEED)

	Player.set_scale(Vector2(scale_x, scale_y))
