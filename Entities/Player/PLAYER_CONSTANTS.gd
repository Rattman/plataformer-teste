extends Node

const GRAVITY = 60

const V_JUMP_HEIGHT = 700

const WALK_MAX_SPEED = 100
const WALK_ACCEL = 10
const WALK_FRICTION = 1.2

const RUN_MAX_SPEED = 700
const RUN_MAX_ACCEL = 30
const RUN_ACCEL_INC = 0.01
const RUN_FRICTION = 1.4

const AIR_ACCEL = 10
const AIR_FRICTION = 1.02

const SLIDE_FRICTION = 1.03

const CROUCH_MAX_SPEED = 50
const CROUCH_ACCEL = 10
const CROUCH_FRICTION = 1.5

const WALL_MAX_SPEED = 10
const WALL_FRICTION = 100
const WALL_ACCEL = 0.5