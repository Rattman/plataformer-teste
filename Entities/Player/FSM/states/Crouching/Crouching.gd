extends "res://addons/net.kivano.fsm/content/FSMState.gd";
################################### R E A D M E ##################################
# For more informations check script attached to FSM node
#
#

##################################################################################
#####  Variables (Constants, Export Variables, Node Vars, Normal variables)  #####
######################### var myvar setget myvar_set,myvar_get ###################
const CONST = preload("../../../PLAYER_CONSTANTS.gd")

enum mv_dir {NONE, LEFT, RIGHT}

var MAX_SPEED
var FRICTION
var ACCEL

var Player

##################################################################################
#########                       Getters and Setters                      #########
##################################################################################
#you will want to use those
func getFSM(): return fsm; #defined in parent class
func getLogicRoot(): return logicRoot; #defined in parent class

##################################################################################
#########                 Implement those below ancestor                 #########
##################################################################################
#you can transmit parameters if fsm is initialized manually
func stateInit(inParam1=null,inParam2=null,inParam3=null,inParam4=null, inParam5=null):
	Player = getLogicRoot()
	MAX_SPEED = CONST.CROUCH_MAX_SPEED
	FRICTION = CONST.CROUCH_FRICTION
	ACCEL = CONST.CROUCH_ACCEL

#when entering state, usually you will want to reset internal state here somehow
func enter(fromStateID=null, fromTransitionID=null, inArg0=null,inArg1=null, inArg2=null):
	getLogicRoot().set_color(Color(1,0,1))

#when updating state, paramx can be used only if updating fsm manually
func update(deltaTime, param0=null, param1=null, param2=null, param3=null, param4=null):
	var speed =  Player.get_v_speed()

	if Player.get_mv_dir() == mv_dir.LEFT:
		speed -= ACCEL
	elif Player.get_mv_dir() == mv_dir.RIGHT:
		speed += ACCEL
	elif Player.get_mv_dir() == mv_dir.NONE:
		speed /= FRICTION
		if abs(speed) < ACCEL: speed = 0

	speed = min(speed, MAX_SPEED)
	speed = max(speed, -MAX_SPEED)

	Player.set_v_speed(speed)

#when exiting state
func exit(toState=null):
	pass

##################################################################################
#########                       Connected Signals                        #########
##################################################################################

##################################################################################
#########     Methods fired because of events (usually via Groups interface)  ####
##################################################################################

##################################################################################
#########                         Public Methods                         #########
##################################################################################

##################################################################################
#########                         Inner Methods                          #########
##################################################################################

##################################################################################
#########                         Inner Classes                          #########
##################################################################################
