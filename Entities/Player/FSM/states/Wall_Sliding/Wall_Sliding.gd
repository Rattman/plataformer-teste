extends "res://addons/net.kivano.fsm/content/FSMState.gd";
################################### R E A D M E ##################################
# For more informations check script attached to FSM node
#
#

##################################################################################
#####  Variables (Constants, Export Variables, Node Vars, Normal variables)  #####
######################### var myvar setget myvar_set,myvar_get ###################
const CONST = preload("../../../PLAYER_CONSTANTS.gd")

enum dir {NONE, LEFT, RIGHT}

var Player
var wall_dir

var ACCEL
var MAX_SPEED
var FRICTION
var GRAVITY

var counter = 0

signal wall_slide_dir(dir)

##################################################################################
#########                       Getters and Setters                      #########
##################################################################################
#you will want to use those
func getFSM(): return fsm; #defined in parent class
func getLogicRoot(): return logicRoot; #defined in parent class

##################################################################################
#########                 Implement those below ancestor                 #########
##################################################################################
#you can transmit parameters if fsm is initialized manually
func stateInit(inParam1=null,inParam2=null,inParam3=null,inParam4=null, inParam5=null):
	Player = getLogicRoot()
	MAX_SPEED = CONST.WALL_MAX_SPEED
	FRICTION = CONST.WALL_FRICTION
	ACCEL = CONST.WALL_ACCEL
	GRAVITY = CONST.GRAVITY

#when entering state, usually you will want to reset internal state here somehow
func enter(fromStateID=null, fromTransitionID=null, inArg0=null,inArg1=null, inArg2=null):
	getLogicRoot().set_color(Color(0.5,0.5,0.5))

	if Player.get_h_speed() < 0:
		Player.set_h_speed(0)

	wall_dir = Player.get_mv_dir()

	emit_signal("wall_slide_dir", wall_dir)

	if wall_dir == dir.LEFT:
		Player.set_v_speed(-10)
	elif wall_dir == dir.RIGHT:
		Player.set_v_speed(10)
	else:
		Player.set_v_speed(0)


#when updating state, paramx can be used only if updating fsm manually
func update(deltaTime, param0=null, param1=null, param2=null, param3=null, param4=null):
	var speed =  Player.get_v_speed()

	if Player.get_mv_dir() == dir.LEFT:
		speed -= ACCEL
	elif Player.get_mv_dir() == dir.RIGHT:
		speed += ACCEL

	speed = min(speed, MAX_SPEED)
	speed = max(speed, -MAX_SPEED)

	Player.set_v_speed(speed)

	var h_speed = Player.get_h_speed()

	h_speed += GRAVITY/FRICTION

	Player.set_h_speed(h_speed)

#when exiting state
func exit(toState=null):
	pass

##################################################################################
#########                       Connected Signals                        #########
##################################################################################

##################################################################################
#########     Methods fired because of events (usually via Groups interface)  ####
##################################################################################

##################################################################################
#########                         Public Methods                         #########
##################################################################################

##################################################################################
#########                         Inner Methods                          #########
##################################################################################

##################################################################################
#########                         Inner Classes                          #########
##################################################################################
