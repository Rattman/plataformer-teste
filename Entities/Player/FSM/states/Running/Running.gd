extends "res://addons/net.kivano.fsm/content/FSMState.gd";
################################### R E A D M E ##################################
# For more informations check script attached to FSM node
#
#

##################################################################################
#####  Variables (Constants, Export Variables, Node Vars, Normal variables)  #####
######################### var myvar setget myvar_set,myvar_get ###################

const CONST = preload("../../../PLAYER_CONSTANTS.gd")

enum mv_dir {NONE, LEFT, RIGHT}

var MAX_SPEED
var FRICTION
var MAX_ACCEL
var ACCEL_INC
var INI_ACCEL

var accel
var dir

var Player = getLogicRoot()

##################################################################################
#########                       Getters and Setters                      #########
##################################################################################
#you will want to use those
func getFSM(): return fsm; #defined in parent class
func getLogicRoot(): return logicRoot; #defined in parent class

##################################################################################
#########                 Implement those below ancestor                 #########
##################################################################################
#you can transmit parameters if fsm is initialized manually
func stateInit(inParam1=null,inParam2=null,inParam3=null,inParam4=null, inParam5=null):
	Player = getLogicRoot()
	MAX_SPEED = CONST.RUN_MAX_SPEED
	FRICTION = CONST.RUN_FRICTION
	INI_ACCEL = CONST.WALK_ACCEL
	ACCEL_INC = CONST.RUN_ACCEL_INC
	MAX_ACCEL = CONST.RUN_MAX_ACCEL

#when entering state, usually you will want to reset internal state here somehow
func enter(fromStateID=null, fromTransitionID=null, inArg0=null,inArg1=null, inArg2=null):
	getLogicRoot().set_color(Color(1,0,0))
	accel = INI_ACCEL
	dir = mv_dir.RIGHT
	if Player.get_v_speed() < 0:
		accel = -accel
		dir = mv_dir.LEFT

#when updating state, paramx can be used only if updating fsm manually
func update(deltaTime, param0=null, param1=null, param2=null, param3=null, param4=null):
	var speed = Player.get_v_speed()
	var player_dir = Player.get_mv_dir()

	if player_dir == mv_dir.LEFT:
		accel -= ACCEL_INC
		speed += accel
	elif player_dir == mv_dir.RIGHT:
		accel += ACCEL_INC
		speed += accel

	if dir != player_dir:
		speed /= FRICTION

	accel = min(accel, MAX_ACCEL)
	accel = max(accel, -MAX_ACCEL)

	speed = min(speed, MAX_SPEED)
	speed = max(speed, -MAX_SPEED)

	Player.set_v_speed(speed)

#when exiting state
func exit(toState=null):
	pass

##################################################################################
#########                       Connected Signals                        #########
##################################################################################

##################################################################################
#########     Methods fired because of events (usually via Groups interface)  ####
##################################################################################

##################################################################################
#########                         Public Methods                         #########
##################################################################################

##################################################################################
#########                         Inner Methods                          #########
##################################################################################

##################################################################################
#########                         Inner Classes                          #########
##################################################################################
