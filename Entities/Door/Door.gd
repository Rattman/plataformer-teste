extends StaticBody2D

export(bool) var closed : bool = true

func _ready():
	if not closed:
		self.set_modulate(Color(1,1,1,0.15))
		$Collision.disabled = true

func activate():
	$Collision.call_deferred("set", "disabled", closed)

	var new_modulate : Color

	if closed:
		new_modulate = Color(1,1,1,0.15)
	else:
		new_modulate = Color(1,1,1,0.9)

	$Tween.interpolate_property(self, "modulate", self.modulate, new_modulate, 0.5, Tween.TRANS_CUBIC, Tween.EASE_OUT)
	$Tween.start()